package br.com.galgo.testes.suite;

import org.junit.BeforeClass;
import org.junit.experimental.categories.Categories;
import org.junit.experimental.categories.Categories.IncludeCategory;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import br.com.galgo.TesteExpurgo;
import br.com.galgo.TesteTarifas;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.utils.SuiteUtils;

@RunWith(Categories.class)
@IncludeCategory(TesteTarifas.class)
@Suite.SuiteClasses({ TesteExpurgo.class })
public class SuiteTarifas {

	private static final String PASTA_SUITE = "Expurgo/Tarifas";

	@BeforeClass
	public static void setUp() throws Exception {
		SuiteUtils.configurarSuiteDefault(Ambiente.PRODUCAO, PASTA_SUITE);
	}

}
